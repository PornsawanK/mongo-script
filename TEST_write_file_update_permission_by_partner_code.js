fs = require("fs");

/// Script migrate permissions by partner code
let temp = [];
// 5) ขาย (4)
const ids5 = ["17990000", "10035415", "10025529", "10035407"];
for (id of ids5) {
  temp.push(
    `db.user_permission_by_partner_code.updateMany(
            { partner_code: "${id}" },
            { $addToSet: { menu: { $each: [NumberInt(4)] } } },
            {
              upsert: true,
            }
          );`
  );
}

// 6) ย้ายค่าย (3)
const ids6 = ["17990000", "10035415", "10025529", "10035406", "10035407"];
for (id of ids6) {
  temp.push(
    `db.user_permission_by_partner_code.updateMany(
                { partner_code: "${id}" },
                { $addToSet: { menu: { $each: [NumberInt(3)] } } },
                {
                  upsert: true,
                }
              );`
  );
}

// 7) โปรรายเดือนขายดี (6)
const ids7 = ["10025529"];
for (id of ids7) {
  temp.push(
    `db.user_permission_by_partner_code.updateMany(
    { partner_code: "${id}" },
    { $addToSet: { menu: { $each: [NumberInt(6)] } } },
    {
      upsert: true,
    }
  );`
  );
}

// 8) เครื่องพร้อมซิมรายเดือน (9)
const ids8 = ["10025529", "10035407", "10035406"];
for (id of ids8) {
  temp.push(
    `   db.user_permission_by_partner_code.updateMany(
    { partner_code: "${id}" },
        { $addToSet: { menu: { $each: [NumberInt(9)] } } },
        {
        upsert: true,
        }
    );`
  );
}

// 10) เปลี่ยนโปรฮัลโหล (29)
const ids10 = ["140"];
for (id of ids10) {
  temp.push(
    `db.user_permission_by_partner_code.updateMany(
    { partner_code: "${id}" },
    { $addToSet: { menu: { $each: [NumberInt(29)] } } },
    {
      upsert: true,
    }
  );`
  );
}

// 13) บริการหลังการขาย (108)
const ids13 = ["17990000", "10035415"];
for (id of ids13) {
  temp.push(
    `  db.user_permission_by_partner_code.updateMany(
    { partner_code: "${id}" },
    { $addToSet: { menu: { $each: [NumberInt(108)] } } },
    {
      upsert: true,
    }
  );`
  );
}

// 23) Partner Grading (107)
const ids23 = ["18801665"];
for (id of ids23) {
  temp.push(
    ` db.user_permission_by_partner_code.updateMany(
    { partner_code: "${id}" },
    { $addToSet: { menu: { $each: [NumberInt(107)] } } },
    {
      upsert: true,
    }
  );`
  );
}

// 28) รายงานสถานะการเปิดเบอร์ (15)
const ids28 = ["10035407", "10035406"];
for (id of ids28) {
  temp.push(
    `db.user_permission_by_partner_code.updateMany(
    { partner_code: "${id}" },
    { $addToSet: { menu: { $each: [NumberInt(15)] } } },
    {
      upsert: true,
    }
  );`
  );
}

fs.writeFile(
  "___update-permission-by-partner-code.js",
  temp.join(" "),
  function (err) {
    if (err) return console.log(err);
    console.log("hello > hello.txt");
  }
);
