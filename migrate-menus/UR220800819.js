for (id of ["1501", "1502"]) {
  db.user_permission.updateMany(
    { partner_type_id: id },
    {
      $addToSet: {
        menu: {
          $each: [
            NumberInt(1),
            NumberInt(27),
            NumberInt(26),
            NumberInt(28),
            NumberInt(2),
            NumberInt(3),
            NumberInt(6),
            NumberInt(9),
            NumberInt(8),
            NumberInt(29),
            NumberInt(7),
            NumberInt(19),
            NumberInt(30),
            NumberInt(31),
            NumberInt(32),
            NumberInt(44),
            NumberInt(50),
            NumberInt(51),
            NumberInt(52),
            NumberInt(15),
            NumberInt(16),
            NumberInt(54),
            NumberInt(55),
            NumberInt(34),
            NumberInt(33),
          ],
        },
        favorite: {
          $each: [
            NumberInt(1),
            NumberInt(27),
            NumberInt(26),
            NumberInt(28),
            NumberInt(2),
            NumberInt(44),
          ],
        },
      },
    }
  );
}
