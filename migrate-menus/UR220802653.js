for (id of ["596", "388", "1494", "1498", "1496"]) {
  db.user_permission.updateMany(
    { partner_type_id: id },
    {
      $addToSet: {
        menu: {
          $each: [
            NumberInt(4),
            NumberInt(3),
            NumberInt(6),
            NumberInt(9),
            NumberInt(8),
            NumberInt(5),
            NumberInt(50),
            NumberInt(15),
            NumberInt(16),
            NumberInt(105),
          ],
        },
        favorite: {
          $each: [NumberInt(4), NumberInt(3)],
        },
      },
    },
    {
      upsert: true,
    }
  );
}

for (id of ["439", "436", "389", "438", "17", "437", "54"]) {
  db.user_permission.updateMany(
    { partner_type_id: id },
    {
      $addToSet: {
        menu: {
          $each: [
            NumberInt(4),
            NumberInt(3),
            NumberInt(6),
            NumberInt(9),
            NumberInt(8),
            NumberInt(15),
            NumberInt(16),
          ],
        },
        favorite: {
          $each: [NumberInt(4), NumberInt(3)],
        },
      },
    },
    {
      upsert: true,
    }
  );
}

for (id of ["460", "466", "575", "781", "459"]) {
  db.user_permission.updateMany(
    { partner_type_id: id },
    {
      $addToSet: {
        menu: {
          $each: [
            NumberInt(4),
            NumberInt(3),
            NumberInt(9),
            NumberInt(8),
            NumberInt(15),
            NumberInt(16),
          ],
        },
        favorite: {
          $each: [NumberInt(4), NumberInt(3)],
        },
      },
    },
    {
      upsert: true,
    }
  );
}

db.user_permission.updateMany(
  { partner_type_id: "597" },
  {
    $addToSet: {
      menu: {
        $each: [
          NumberInt(4),
          NumberInt(3),
          NumberInt(9),
          NumberInt(8),
          NumberInt(5),
          NumberInt(15),
          NumberInt(16),
          NumberInt(105),
        ],
      },
      favorite: {
        $each: [NumberInt(4), NumberInt(3)],
      },
    },
  },
  {
    upsert: true,
  }
);

db.user_permission.updateMany(
  { partner_type_id: "1497" },
  {
    $addToSet: {
      menu: {
        $each: [
          NumberInt(4),
          NumberInt(3),
          NumberInt(6),
          NumberInt(9),
          NumberInt(8),
          NumberInt(5),
          NumberInt(15),
          NumberInt(16),
          NumberInt(105),
        ],
      },
      favorite: {
        $each: [NumberInt(4), NumberInt(3)],
      },
    },
  },
  {
    upsert: true,
  }
);

db.user_permission.updateMany(
  { partner_type_id: "1495" },
  {
    $addToSet: {
      menu: {
        $each: [
          NumberInt(4),
          NumberInt(3),
          NumberInt(6),
          NumberInt(9),
          NumberInt(8),
          NumberInt(5),
          NumberInt(50),
          NumberInt(15),
          NumberInt(16),
        ],
      },
      favorite: {
        $each: [NumberInt(4), NumberInt(3)],
      },
    },
  },
  {
    upsert: true,
  }
);
