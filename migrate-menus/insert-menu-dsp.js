db.user_menu.insertOne({
  group: "รายงาน",
  icon: "https://tcc-dealer-ext.truecorp.co.th/public/tccext/assets/image/menu/ico-sim-active.png",
  id: NumberInt(117),
  label: "รายงานสรุปยอดการขาย",
  module: "report-hierarchy-transaction",
  priority: 99,
  type: "flutter",
});
