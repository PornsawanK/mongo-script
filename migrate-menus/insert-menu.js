db.user_menu.insert([
    {
    "group" : "ขายและบริการ",
    "icon" : "https://tcc-dealer-ext.truecorp.co.th/public/tccext/assets/image/menu/ico-pretty-number.png",
    "id" : NumberInt(111),
    "label" : "จองและขายเบอร์สวย(ซิมเติมเงิน)",
    "priority" : NumberInt(41),
    "type" : "webview",
    "url" : "https://numberallocated-dealer.truecorp.co.th/prepaid-premium-number/web"
}, {
    "group" : "บริหารร้านค้าและตัวแทน L2",
    "icon" : "https://tcc-dealer-ext.truecorp.co.th/public/tccext/assets/image/menu/ico-report-s2.png",
    "id" : NumberInt(112),
    "label" : "เข้าเยี่ยมเพื่อนคู่ค้า (Check-in)",
    "module" : "cprm-bp-check-in-mobile",
    "path" : "/cprm-bp-check-in-mobile/index.html",
    "priority" : NumberInt(41),
    "type" : "zip",
    "zip" : "https://cprm.truecorp.co.th/resources/ui/cprm-bp-check-in-mobile.zip.enc",
    "version" : "1.0",
    "md5" : "37e03d3de8bb662e837962f825551e33"
}, {
    "group" : "รายงาน",
    "icon" : "https://tcc-dealer-ext.truecorp.co.th/public/tccext/assets/image/menu/ico-report-s2.png",
    "id" : NumberInt(113),
    "label" : "รายงาน S2 ที่อยู่ภายใต้ Hierarchy",
    "module" : "cprm-report-web",
    "path" : "/cprm-report-web/index.html#/s2hierarchy",
    "priority" : NumberInt(41),
    "type" : "zip",
    "zip" : "https://cprm.truecorp.co.th/resources/ui/cprm-report-web.zip.enc",
    "version" : "1.0",
    "md5" : "227b49d48792ad93d5fa00251e8aa337"
},
{
    "group" : "รายงาน",
    "icon" : "https://tcc-dealer-ext.truecorp.co.th/public/tccext/assets/image/menu/ico-overdue-payment.png",
    "id" : NumberInt(114),
    "label" : "Overdue Payment Report",
    "module" : "cprm_overdue_mobile",
    "path" : "/cprm_overdue_mobile/index.html",
    "priority" : NumberInt(41),
    "type" : "zip",
    "zip" : "https://cprm.truecorp.co.th/resources/ui/cprm-overdue-dealer-mobile.zip.enc",
    "version" : "1.0",
    "md5" : "d9208a39acc13bd892c76ac0795b5b96",
}
])