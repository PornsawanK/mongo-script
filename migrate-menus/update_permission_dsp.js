const ids = [
  "367",
  "724",
  "884",
  "885",
  "1048",
  "1049",
  "1448",
  "1471",
  "848",
  "850",
  "1030",
  "1079",
  "1481",
  "725",
  "1449",
  "59",
];

for (id of ids) {
  db.user_permission.updateMany(
    { partner_type_id: id },
    { $addToSet: { menu: { $each: [NumberInt(117)] } } },
    {
      upsert: true,
    }
  );
}
