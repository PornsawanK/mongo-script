// convert excel to json https://products.aspose.app/cells/conversion/excel-to-json

const exampleLanguageLabel = [
  {
    id: 21,
    th: "yyy1",
    mm: "xxx1",
  },
  {
    id: 24,
    th: "yyy2",
    mm: "xxx2",
  },
];

function formatLabelLanguage(languageLabel) {
  let result = [];
  languageLabel.map((item) => {
    result.push({
      id: item.id,
      labelLanguage: {
        th: item.th,
        mm: item.mm,
      },
    });
  });
  return result;
}

// console.log(formatMongo(exampleLanguageLabel));
