// This is an example. Don't forget to change to real data it should be export from CSV File
// convert excel to json https://products.aspose.app/cells/conversion/excel-to-json
const menus = [
  {
    id: 1,
    labelLanguage: {
      th: "ลงทะเบียนซิมเติมเงิน1",
      en: "prepaid register1",
      mm: "ငွေဖြည့်ဆင်းမ်ကတ်ကို မှတ်ပုံတင်ပါ။",
    },
  },
  {
    id: 2,
    labelLanguage: {
      th: "ลงทะเบียนซิมเติมเงิน2",
      en: "prepaid register2",
      mm: "ငွေဖြည့်ဆင်းမ်ကတ်ကို မှတ်ပုံတင်ပါ။",
    },
  },
  {
    id: 3,
    labelLanguage: {
      th: "ลงทะเบียนซิมเติมเงิน3",
      en: "prepaid register3",
      mm: "ငွေဖြည့်ဆင်းမ်ကတ်ကို မှတ်ပုံတင်ပါ။",
    },
  },
];

for (menu of menus) {
  db.user_menu.updateMany(
    { id: menu.id },
    { $set: { labelLanguage: menu.labelLanguage } }
  );
}
